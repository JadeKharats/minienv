require "spec"
require "../../src/config/config"

describe Config do
  describe "#server_host" do
    it "return 0.0.0.0 by default" do
      ENV.delete("HOST")
      config.server_host.should eq "0.0.0.0"
    end
    it "return value in environment variable" do
      ENV["HOST"] = "pikachu.poke.mon"
      config.server_host.should eq "pikachu.poke.mon"
    end
  end
  describe "#server_port" do
    it "return 8080 by default" do
      ENV.delete("PORT")
      config.server_port.should eq 8080
    end
    it "return value in environment variable" do
      ENV["PORT"] = "3000"
      config.server_port.should eq 3000
    end
  end
  describe "#database_url" do
    it "return basis url by default" do
      ENV.delete("MYSQL_PORT")
      ENV.delete("MYSQL_HOST")
      ENV.delete("MYSQL_USER")
      ENV.delete("MYSQL_PASSWORD")
      ENV.delete("MYSQL_DATABASE")
      config.database_url.should eq "mysql://minienv:minienvpass@localhost:3306/db"
    end
    it "return url from param in environment variable" do
      ENV["MYSQL_PORT"] = "3456"
      ENV["MYSQL_HOST"] = "plop.com"
      ENV["MYSQL_USER"] = "envuser"
      ENV["MYSQL_PASSWORD"] = "envpass"
      ENV["MYSQL_DATABASE"] = "database"
      config.database_url.should eq "mysql://envuser:envpass@plop.com:3456/database"
    end
  end
end
