class Server
  def self.run
    server = HTTP::Server.new do |context|
      dbtest = DatabaseTest.new
      dbtest.load
      output = "%s\n%s\n%s" % [InformationSystem.print, dbtest.count, Minienvdb::VERSION]
      context.response.content_type = "text/plain"
      context.response.print output
    end

    server.bind_tcp config.server_host, config.server_port
    server.listen
  end
end
