module InformationSystem
  extend self

  def self.print
    output = String.new
    ENV.each do |variable, valeur|
      output += "%s : %s \n" % [variable, valeur]
    end
    output
  end
end
