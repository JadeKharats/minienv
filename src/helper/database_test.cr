class DatabaseTest
  def load
    query = "DROP TABLE IF EXISTS pokemon"
    sql.exec query

    query = "CREATE TABLE IF NOT EXISTS pokemon (nom VARCHAR(255), evolution VARCHAR(255));"
    sql.exec(query)

    (1..25).each do |index|
      query = "INSERT INTO pokemon VALUES ('pokemon?','pokemon?')"
      sql.exec query, index, (index + 1)
    end
  end

  def count
    sql.scalar "SELECT count(1) FROM pokemon"
  end
end
