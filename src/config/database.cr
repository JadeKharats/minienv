require "db"
require "mysql"

class SQLServer
  getter sql : DB::Database

  def initialize
    @sql = DB.open config.database_url
    Log.info &.emit("Connexion database ouverte", connexion: @sql.uri.to_s)
  end

  def self.instance
    @@instance ||= SQLServer.new
  end
end

def sql
  SQLServer.instance.sql
end
