class Config
  def database_url
    host = ENV.fetch("MYSQL_HOST", "localhost")
    port = ENV.fetch("MYSQL_PORT", "3306").to_i
    user = ENV.fetch("MYSQL_USER", "minienv")
    password = ENV.fetch("MYSQL_PASSWORD", "minienvpass")
    database = ENV.fetch("MYSQL_DATABASE", "db")
    url = "mysql://%s:%s@%s:%s/%s" % [user, password, host, port, database]
    ENV.fetch("DATABASE_URL", url)
  end

  def server_host
    ENV.fetch("HOST", "0.0.0.0")
  end

  def server_port
    ENV.fetch("PORT", "8080").to_i
  end

  def self.instance
    @@instance ||= Config.new
  end
end

def config
  Config.instance
end
